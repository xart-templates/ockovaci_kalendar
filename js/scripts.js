$(document).ready(function(){

	$('.header_logo').after('<button class="toggle_menu"/>');
	$('.toggle_menu').off().on('click',function(){
		$('body').toggleClass('menu-on');
	});
	// checkbox, radio style
	$('[type="checkbox"],[type="radio"]').wrap('<span class="checkbox_wrap"/>').after('<span class="checkbox_btn"/>');

	// select box
	$('select').wrap('<span class="select_wrap"/>');

	$('.box_slideshow').each(function(){
		var i = $('.item',this).length;
		if(i>1){
			$(this).append('<div class="container"><div class="pager"/></div>');
			$('.items',this).cycle({
				slides: '.item',
				speed: 500,
				timeout: 6000,
				pauseOnHover: true,
				swipe: true,
				pager: $('.pager',this),
			});
		}
	});


	// file input style
	$('input[type=file]').each(function(){
		var uploadText = $(this).data('value');
		if (uploadText) {
			var uploadText = uploadText;
		} else {
			var uploadText = 'Choose file';
		}
		var inputClass=$(this).attr('class');
		$(this).wrap('<span class="fileinputs"></span>');
		$(this)
			.parents('.fileinputs')
			.append($('<span class="fakefile"/>')
			.append($('<input class="input-file-text" type="text" />')
			.attr({'id':$(this)
			.attr('id')+'__fake','class':$(this).attr('class')+' input-file-text'}))
			.append('<input type="button" value="'+uploadText+'">'));
		$(this)
			.addClass('type-file')
			.css('opacity',0);
		$(this)
			.bind('change mouseout',function(){$('#'+$(this).attr('id')+'__fake')
			.val($(this).val().replace(/^.+\\([^\\]*)$/,'$1'))
		})
	});



	// placeholder
	if (document.createElement('input').placeholder==undefined){
		$('[placeholder]').focus(function(){
			var input=$(this);
			if(input.val()==input.attr('placeholder')){
				input.val('');
				input.removeClass('placeholder')
			}
		}).blur(function(){
			var input=$(this);
			if(input.val()==''||input.val()==input.attr('placeholder')){
				input.addClass('placeholder');
				input.val(input.attr('placeholder'))
			}
		}).blur()
	}

});